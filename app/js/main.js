$("document").ready(function() {

var ratioWidth=620;

     var windowWidth = document.body.clientWidth;
    var windowHeight = document.body.clientHeight;

    responsive_resize();

    $(window).resize(function(){

        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;

        responsive_resize();
    })

// --------------------

var master = new TimelineMax({
   });

function getTimeline(){
   var timeline = new TimelineMax();
   timeline.to("#svg-bulb .ray", 0.5, {strokeDashoffset: "66", opacity: "0"}, "fadeOut")
      .to("#svg-bulb .ray", 0.5, {strokeDashoffset: "0", opacity: "1"}, "fadeIn")
      .to("#svg-bulb .filament", 0.5, {opacity: "0"}, "fadeOut")
      .to("#svg-bulb .filament", 0.5, {opacity: "1"}, "fadeIn")
      .to(".research-wrapper .text", 0.5, {scale:1, opacity: "1", ease: Elastic. easeOut.config( 1, 1)},'+=0.1')
      .to("#svg-bulb .ray", 0.5, {strokeDashoffset: "66", opacity: "0"}, "fadeOut2")
      .to("#svg-bulb .ray", 0.5, {strokeDashoffset: "0", opacity: "1"}, "fadeIn2")
      .to("#svg-bulb .filament", 0.5, {opacity: "0"}, "fadeOut2")
      .to("#svg-bulb .filament", 0.5, {opacity: "1"}, "fadeIn2");
   return timeline;
}

function getTimeline2(){
   var timeline2 = new TimelineMax();
   timeline2.to("#container #step1 .research-wrapper #line1 .st0",0.5 , {strokeDashoffset: "0"}, "first")
   .to("#container #step1 .research-wrapper #line1 .st1",0.5 , {strokeDashoffset: "0"}, "first")
   .to(".research", 1, {scale:1, ease: Elastic. easeOut.config( 1.5, 0.5)},"first+=0.5")
   .to(".research span", 1, {opacity: "1"}, "first");
   return timeline2;
}

function getTimeline3(){
   var timeline3 = new TimelineMax();
   timeline3.to("#step2 .chart", 0.5, {opacity: "1"})
      .to("#arrowsSection .line", 0.5, {strokeDashoffset: "0"}, "first-circle-arrows")
   .to("#arrowsSection .linehead", 0.5, {strokeDashoffset: "0"}, "first-circle-arrows")
   .to(".arrow-text", 0.5, {opacity: "1"})
   .to(".sections .one", 1, {opacity: "1"}, "final")
   .to(".sections .two", 1, {opacity: "1"}, "final+=1")
   .to(".sections .four", 1, {opacity: "1"}, "final+=2")
   .to(".sections .three", 1, {opacity: "1"}, "final+=3")
   .to("#arrowsSection", 3, {directionalRotation:"270_cw"}, "final+=1");
   return timeline3;
}

function getTimeline4(){
   var timeline4 = new TimelineMax();
   timeline4.to("#step3 .left #Layer_1 .line", 0.5, {strokeDashoffset: "0"}, "first-left-arrow")
   .to("#step3 .left #Layer_1 .arrows", 0.5, {strokeDashoffset: "0"}, "first-left-arrow");

   return timeline4;
}

function getTimeline5(){
   var timeline5 = new TimelineMax();
   timeline5.to("#step3 .numbers", 0.5, {opacity: "1"})
            .staggerTo($("#step3 li svg .st0"), 1, {fill:"#05A7B5", opacity: 1, ease:Power3.easeOut}, 1, "numbersfirst")
            .staggerTo($("#step3 li svg .st1"), 1, {fill:"#ffffff", opacity: 1, ease:Power3.easeOut}, 1, "numbersfirst")
            .staggerTo($("#step3 li .numberfade .st0"), 1,{autoAlpha:0, ease:Power3.easeOut}, 1, "numbersfirst+=1")
            .staggerTo($("#step3 li .numberfade .st1"), 1,{fill:"#6E6E6D", ease:Power3.easeOut}, 1, "numbersfirst+=1");

   return timeline5;
}

function getTimeline6(){
   var timeline6 = new TimelineMax();
   timeline6.to("#step3 .right .line", 0.5, {strokeDashoffset: "1352"}, "first-right-arrow")
   .to($("#step3 .right .arrows"), 0.5, {strokeDashoffset: "0", ease:Power3.easeOut}, "first-right-arrow");

   return timeline6;
}

function getTimeline7(){
   var timeline7 = new TimelineMax();
   timeline7
   .to("#step3 .portfolio .st0", 1, {strokeDashoffset: "0"}, "case-first")
   .to("#step3 .portfolio .st1", 1, {strokeDashoffset: "0"}, "case-first")
   .to("#step3 .portfolio .st2", 1, {strokeDashoffset: "0"}, "case-first")
   .to("#step3 .portfolio .text", 1, {scale:1, opacity: "1", ease: Elastic. easeOut.config( 1, 1)},'case-first+=0.1');
   return timeline7;
}



function init(){   
   master.add(getTimeline(), 'scene-intro')
   master.add(getTimeline2(), 'second')
   master.add(getTimeline3(), 'third')
   master.add(getTimeline4(), 'fourth')
   master.add(getTimeline5(), 'fifth')
   master.add(getTimeline6(), 'sixth')
    master.add(getTimeline7(), 'seventh');}
init();

// -------------

// Responsive resize
    function responsive_resize(){
        finalW=$("#outer-wrapper").width();
        scale=finalW/ratioWidth;
        scale=scale>1?1:scale;
        TweenMax.set("#resizer",{scale:scale});
        $('#scalingContainer').height($("#resizer")[0].getBoundingClientRect().height);
    } 



// Get SVG Path length
var path = document.querySelector("#container #step3 .right .line");
var total_length = path.getTotalLength();
console.log(total_length)



});