# Steps for installation

* Run the `npm install` command after cloning this repo
* Run the `gulp` command for the live sync
* Run the `gulp build` to generate the dist folder

# Folder use

* All work should be done in the "app" folder
* The "SCSS" folder should be used for editing styles
* The "CSS" folder is the resulting SCSS files
* Include all CSS and JS files on the index.html to preview the changes


# Useful links

* https://greensock.com/docs/TimelineMax/call
* https://ihatetomatoes.net/wp-content/uploads/2016/07/GreenSock-Cheatsheet-4.pdf
* https://greensock.com/docs/Easing
* https://www.sitepoint.com/crash-course-optimizing-and-exporting-svgs-in-adobe-illustrator/
* https://codepen.io/MyXoToD/post/howto-self-drawing-svg-animation